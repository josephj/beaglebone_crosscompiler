x# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  config.vm.box = "hashicorp/precise64"
  config.vm.hostname = "rustyBox"
  config.vm.network "public_network", bridge: "en0: Wi-Fi (AirPort)"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
   config.vm.synced_folder "./data", "/vagrant_data"


  # Enable provisioning with a shell script. Additional provisioners such as
  # Puppet, Chef, Ansible, Salt, and Docker are also available. Please see the
  # documentation for more information about their specific syntax and use.
   config.vm.provision "shell", inline: <<-SHELL
     sudo apt-get update -y
     sudo apt-get install -y curl git

     #Install Rust
     curl https://sh.rustup.rs -sSf | sh
    
     #Install the C cross toolchain
     apt-get install -y -qq gcc-arm-linux-gnueabihf emacs
     
     #Install the cross compiled standard crates
     rustup target add armv7-unknown-linux-gnueabihf

     #configure cargo for cross compilation
     mkdir -p ~/.cargo
     cat >>~/.cargo/config <<EOF
         [target.armv7-unknown-linux-gnueabihf]
         linker = "arm-linux-gnueabihf-gcc"
         EOF
   SHELL
end
